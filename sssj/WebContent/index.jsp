<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
 String host = request.getHeader("host");
 String path = "http://" + host + request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>欢迎界面</title>
</head>
<body>
	<h1>Holle SpringMVC + Spring + Shiro + JPA</h1>
	<br>
	<div style="color: red; font-size: 22px;">${message_login}</div>
	<form action="toLogin" method="post">
		用户名称：<input type="text" name="username"/>
		密码：<input type="text" name="password"/>
		下次自动登录：<input type="checkbox" name="rememberMe" value="true">
		
		<input type="submit" value="登录"/>
		<input type="reset" value="清空"/>
	</form>
	<br/>
		
	<a href="<%=request.getContextPath()%>/toRegist">去注册</a>
	
</body>
</html>