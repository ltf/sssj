package main.java.report;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

public class OrderDataSource<ItemStatReport> extends JRAbstractBeanDataSource {
	
	private Object currentBean;
	private Iterator<ItemStatReport> iterator;
	private List<ItemStatReport> allData;
	private int index = -1;

	public OrderDataSource(List<ItemStatReport> collection) {
		super(true);
		if (collection.size() > 0) {
			this.allData = collection;
			this.iterator = allData.iterator();
		}
	}
	
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		Object value = null;
		value = getFieldValue(currentBean, field);
		return value;
	}

	@Override
	public boolean next() throws JRException {
		index++;
		boolean hasNext = false;
		if (this.iterator != null) {
			hasNext = this.iterator.hasNext();
			if (hasNext) {
				this.currentBean = this.iterator.next();
			}
		}
		return hasNext;
	}

	@Override
	public void moveFirst() throws JRException {
		if (this.allData != null) {
			this.iterator = this.allData.iterator();
		}
	}

	public Collection<ItemStatReport> getData() {
		return allData;
	}
 
	public int getRecordCount() {
		return allData == null ? 0 : allData.size();
	} 
	
 	@SuppressWarnings({ "rawtypes", "unchecked" })
	public OrderDataSource cloneDataSource() {
		return new OrderDataSource(allData);
	}
}
