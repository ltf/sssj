package main.java.general.realm;

import java.util.ArrayList;
import java.util.List;

import main.java.login.entity.OPermissionPO;
import main.java.login.entity.ORolePO;
import main.java.login.entity.OUserPO;
import main.java.login.service.IRolePermissionRelationService;
import main.java.login.service.IUserRoleRelationService;
import main.java.login.service.IUserService;
import main.java.util.JSONUtil;
import main.java.util.StringUtil;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class UserRealm extends AuthorizingRealm implements ApplicationContextAware{

	@Autowired
	private IUserService iUserService;
	
	@Autowired
	private IRolePermissionRelationService iRolePermissionRelationService;
	
	@Autowired
	private IUserRoleRelationService iUserRoleRelationService;
	
	private ApplicationContext applicationContext;
	
	private Class<?> providorInterface;
	
	public void setProvidorInterface(Class<?> providorInterface) {
		this.providorInterface = providorInterface;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	
	/*
	 * 如果跳转的界面中有权限的标签验证，那么会通过该方法进行权限的赋予
	 * (non-Javadoc)
	 * @see org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();
		
		System.out.println("doGetAuthorizationInfo...获取权限/授权");

		List<String> permissionList = new ArrayList<String>();
		
		// 获取当前登录的用户名,等价于(String)principalCollection.fromRealm(this.getName()).iterator().next()
		String currentUsername = (String) super.getAvailablePrincipal(principalCollection);
//		OUserPO userPO = (OUserPO)principalCollection.fromRealm(getName()).iterator().next();
		//从数据库中获取当前登录用户的详细信息
//		OUserPO currentUserPO = (OUserPO) SecurityUtils.getSubject().getSession().getAttribute("user");
		
//		OUserPO currentUserPO = iUserService.getUserById(userPO.getId());
		OUserPO currentUserPO = iUserService.getUserPObyUsername(currentUsername);
		
		//获取实体类User中包含有用户角色的实体类信息
		ORolePO rolePO = iUserRoleRelationService.getRolePOByUserId(currentUserPO.getId());
		
		//获取权限
		List<OPermissionPO> permissionPOs = iRolePermissionRelationService.getPermissionPOsByRoleId(rolePO.getId());
		for(OPermissionPO po : permissionPOs){
			permissionList.add(po.getPermissionName());
		}
		
		//为当前用户设置角色和权限
		simpleAuthorInfo.addRole(rolePO.getRolename());
		simpleAuthorInfo.addStringPermissions(permissionList);
System.out.println("授权--->" + JSONUtil.toJSON(simpleAuthorInfo));
		return simpleAuthorInfo;
	}

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken authenticationToken) throws AuthenticationException {
		
		System.out.println("doGetAuthenticationInfo...认证回调函数,登录时调用.");
		
		String username = (String) authenticationToken.getPrincipal();
		String password = new String((char[]) authenticationToken.getCredentials());
		
		OUserPO userPO = iUserService.login(username, password);
System.out.println("登录查询用户---->" + JSONUtil.toJSON(userPO));		
		if(!StringUtil.isEmpty(userPO)){
			AuthenticationInfo authcInfo = new SimpleAuthenticationInfo(username,password,ByteSource.Util.bytes(userPO.getSalt()),this.getName());
			this.setSession("user", userPO);
System.out.println("登录返回------>" + JSONUtil.toJSON(authcInfo));
			return authcInfo;
		}
		
		return null;
	}

	/**
	 * 将一些数据放到ShiroSession中,以便于其它地方使用
	 * 
	 * @see 比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到
	 */
	private void setSession(Object key, Object value) {
		Subject currentUser = SecurityUtils.getSubject();
		if (null != currentUser) {
			Session session = currentUser.getSession();
			if (null != session) {
				session.setAttribute(key, value);
			}
		}
	}
	
}
