package main.java.login.service;

import java.util.List;

import main.java.login.entity.OUserPO;
import main.java.login.entity.OUserVO;
import main.java.util.OResultVO;

public interface IUserService {

	public OResultVO toRegistUser(String username, String password);

	public OUserPO login(String username, String password);

	public List<OUserPO> getUsers();

	public List<OUserVO> getUsersVO();

	public OUserPO getUserPObyUsername(String currentUsername);

	public OUserPO getUserById(Long id);

}
