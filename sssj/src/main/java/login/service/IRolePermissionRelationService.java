package main.java.login.service;

import java.util.List;

import main.java.login.entity.OPermissionPO;


public interface IRolePermissionRelationService {

	
	public List<OPermissionPO> getPermissionPOsByRoleId(Long roleId);
	
	
	
}
