package main.java.login.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import main.java.login.dao.IRolePermissionRelationDao;
import main.java.login.entity.OPermissionPO;
import main.java.login.entity.ORolePermissionRelationPO;
import main.java.login.service.IRolePermissionRelationService;
import main.java.util.StringUtil;

@Service
@Transactional
public class RolePermissionRelationServiceImpl implements IRolePermissionRelationService{

	@Autowired
	private IRolePermissionRelationDao iRolePermissionRelationDao;
	
	
	@Override
	public List<OPermissionPO> getPermissionPOsByRoleId(Long roleId) {
		if(StringUtil.isEmpty(roleId)){
			return Collections.emptyList();
		}
		String jpql = " from ORolePermissionRelationPO where rolePO.id = " + roleId;
		List<ORolePermissionRelationPO> list = iRolePermissionRelationDao.findPermissionRelationPOsByJpql(jpql);
		List<OPermissionPO> result = new ArrayList<OPermissionPO>();
		if(!StringUtil.isEmpty(list)){
			for(ORolePermissionRelationPO po : list){
				result.add(po.getPermissionPO());
			}
			return result;
		}
		
		return Collections.emptyList();
	}

}
