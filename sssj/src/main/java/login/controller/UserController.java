package main.java.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.general.enums.FLAGENUM;
import main.java.login.entity.OUserHighChartsVO;
import main.java.login.entity.OUserPO;
import main.java.login.entity.OUserVO;
import main.java.login.service.IUserService;
import main.java.report.service.IJasperReportService;
import main.java.util.OResultVO;
import main.java.util.StringUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Controller
public class UserController {

	private static Log log = LogFactory.getLog(UserController.class);

	@Autowired
	private IUserService iUserService;
	
	@Autowired
	private IJasperReportService iJasperReportService;
	
	@RequestMapping(value = "/toRegist", method = RequestMethod.GET)
	public ModelAndView toRegist() {
		ModelAndView view = new ModelAndView();
		view.setViewName("regist");
		return view;
	}

	/**
	 * 登录
	 * 2014年12月4日 下午4:18:03
	 * @param request
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/toLogin",method=RequestMethod.POST)
	public ModelAndView toLogin(HttpServletRequest request,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "rememberMe", required = false) boolean rememberMe) {

		ModelAndView view = new ModelAndView();

		UsernamePasswordToken token = new UsernamePasswordToken(username,password,rememberMe);

		// 获取当前的Subject
		Subject currentUser = SecurityUtils.getSubject();

		try {
			currentUser.login(token);
		} catch (AuthenticationException e) {
			log.info("用户名或密码不正确..." + e);
			request.setAttribute("message_login", "用户名或密码不正确");
			view.setViewName(InternalResourceViewResolver.FORWARD_URL_PREFIX + "/");
			return view;
		}

System.out.println("验证登录是否成功----->" + currentUser.isAuthenticated());
		
		// 验证是否登录成功
		if (currentUser.isAuthenticated()) {
			view.setViewName("user");
			request.setAttribute("user", currentUser.getSession().getAttribute("user"));
			System.out.println("用户[" + username + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");
			
		} else {
			token.clear();
			view.setViewName(InternalResourceViewResolver.REDIRECT_URL_PREFIX + "/");
		}

		return view;

	}

	/**
	 * 注册
	 * 2014年12月4日 下午4:17:54
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/toAdd")
	public ModelAndView toAdd(
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password) {

		ModelAndView view = new ModelAndView();
		if (StringUtil.isEmpty(username) || StringUtil.isEmpty(password)) {
			view.setViewName("regist");
			return view;
		}

		OResultVO resultVO = iUserService.toRegistUser(username, password);

		if (resultVO.isFlag()) {
			log.info("---------用户【" + username + "】注册成功... ");
			view.setViewName("redirect:/index");
		} else {
			log.info("---------用户【" + username + "】已存在... ");
			view.setViewName("regist");
		}
		return view;

	}

	@RequestMapping("/toLookUsers")
	public String toLookUsers(HttpServletRequest request){
		List<OUserPO> list = iUserService.getUsers();
		request.setAttribute("users", list);
		return "users";
	}
	
	@RequestMapping(value="toGetUsersToShowCharts")
	public @ResponseBody List<OUserHighChartsVO> toGetUsersToShowCharts(){
		List<OUserPO> list = iUserService.getUsers();
		
		int locked = 0;
		int unlocked = 0;
		for(OUserPO po : list){
			if(FLAGENUM.TRUE.equals(po.getLocked())){
				locked += 1;
			}else{
				unlocked += 1;
			}
		}
		
		List<OUserHighChartsVO> result = new ArrayList<OUserHighChartsVO>();
		result.add(new OUserHighChartsVO("锁定", locked));
		result.add(new OUserHighChartsVO("未锁定", unlocked));
		
		return result;
	}
	
	
	/**
	 * 退出
	 * 2014年12月4日 下午4:17:39
	 * @return
	 */
	@RequestMapping("/toLogout")
	public String toLogout(){
		SecurityUtils.getSubject().logout();
		return InternalResourceViewResolver.REDIRECT_URL_PREFIX + "/";
	}
	
	/**
	 * 注解权限控制打印
	 * 2014年12月31日 下午4:31:29
	 * @param request
	 * @param response
	 */
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/toPrint")
	public void toPrint(HttpServletRequest request,HttpServletResponse response){
		List<OUserVO> list = iUserService.getUsersVO();
		iJasperReportService.printUserList(request, response, list);
	}
	
	@RequestMapping(value="/login")
	public String login(){
		return InternalResourceViewResolver.FORWARD_URL_PREFIX + "/index.jsp";
	}
	
}
