package main.java.login.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import main.java.general.enums.FLAGENUM;

/**
 * 用户-角色关系
 * @author cOde mOnkey
 * 2014年12月2日 下午2:49:30
 */
@Entity
@Table(name = "S_USER_ROLE_RELATION")
public class OUserRoleRelationPO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID",referencedColumnName = "ID",insertable = false, updatable = true)
	private OUserPO userPO;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLE_ID",referencedColumnName = "ID", insertable = false, updatable = true)
	private ORolePO rolePO;

	@Column(name = "AVAILABLE")
	@Enumerated(EnumType.STRING)
	private FLAGENUM available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OUserPO getUserPO() {
		return userPO;
	}

	public void setUserPO(OUserPO userPO) {
		this.userPO = userPO;
	}

	public ORolePO getRolePO() {
		return rolePO;
	}

	public void setRolePO(ORolePO rolePO) {
		this.rolePO = rolePO;
	}

	public FLAGENUM getAvailable() {
		return available;
	}

	public void setAvailable(FLAGENUM available) {
		this.available = available;
	}
	
}
