package main.java.login.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import main.java.general.enums.FLAGENUM;

/**
 * 用户
 * @author cOde mOnkey
 * 2014年12月2日 下午2:49:30
 */
@Entity
@Table(name = "S_USER")
public class OUserPO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "SALT")
	private String salt;
	
	@Column(name = "LOCKED")
	@Enumerated(EnumType.STRING)
	private FLAGENUM locked = FLAGENUM.FALSE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public FLAGENUM getLocked() {
		return locked;
	}

	public void setLocked(FLAGENUM locked) {
		this.locked = locked;
	}

	public OUserPO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OUserPO(Long id) {
		super();
		this.id = id;
	}
	

}
