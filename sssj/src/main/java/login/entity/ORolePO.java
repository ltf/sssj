package main.java.login.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import main.java.general.enums.FLAGENUM;

/**
 * 角色
 * @author cOde mOnkey
 * 2014年12月2日 下午2:49:11
 */
@Entity
@Table(name = "S_ROLE")
public class ORolePO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "ROLENAME")
	private String rolename;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "AVAILABLE")
	private FLAGENUM available = FLAGENUM.TRUE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FLAGENUM getAvailable() {
		return available;
	}

	public void setAvailable(FLAGENUM available) {
		this.available = available;
	}

	public ORolePO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ORolePO(Long id) {
		super();
		this.id = id;
	}
	
}
