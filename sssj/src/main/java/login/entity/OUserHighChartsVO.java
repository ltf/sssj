package main.java.login.entity;

import java.io.Serializable;
/**
 * highcharts图表展示vo
 * @author cOde mOnkey
 * 2014年12月10日 下午4:04:29
 */
public class OUserHighChartsVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String showName;
	
	private int number = 0;

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public OUserHighChartsVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OUserHighChartsVO(String showName, int number) {
		super();
		this.showName = showName;
		this.number = number;
	}
	
}
