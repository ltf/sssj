package main.java.login.dao;

import java.util.List;

import main.java.login.entity.ORolePermissionRelationPO;
import main.java.util.BaseDao;

public interface IRolePermissionRelationDao {

	public BaseDao getBaseDao();
	
	public void save(ORolePermissionRelationPO rolePermissionRelationPO);
	
	public ORolePermissionRelationPO update(ORolePermissionRelationPO rolePermissionRelationPO);
	
	public List<ORolePermissionRelationPO> getPermissionRelationPOs(ORolePermissionRelationPO rolePermissionRelationPO);
	
	public List<ORolePermissionRelationPO> findPermissionRelationPOsByJpql(String jpql);
	
}
