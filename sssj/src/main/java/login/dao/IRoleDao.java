package main.java.login.dao;

import main.java.login.entity.ORolePO;
import main.java.util.BaseDao;

public interface IRoleDao {

	public BaseDao getBaseDao();
	
	public void save(ORolePO rolePO);
	
	public ORolePO findRolePOById(Long id);
	
}
