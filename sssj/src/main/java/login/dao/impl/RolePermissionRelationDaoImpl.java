package main.java.login.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.java.login.dao.IRolePermissionRelationDao;
import main.java.login.entity.ORolePermissionRelationPO;
import main.java.util.BaseDao;
import main.java.util.StringUtil;

@Repository
public class RolePermissionRelationDaoImpl implements IRolePermissionRelationDao{

	@Autowired
	private BaseDao baseDao;
	
	@Override
	public BaseDao getBaseDao() {
		return baseDao;
	}

	@Override
	public void save(ORolePermissionRelationPO rolePermissionRelationPO) {
		baseDao.save(rolePermissionRelationPO);
	}

	@Override
	public ORolePermissionRelationPO update(ORolePermissionRelationPO rolePermissionRelationPO) {
		return baseDao.saveOrUpdateEntity(rolePermissionRelationPO);
	}

	@Override
	public List<ORolePermissionRelationPO> getPermissionRelationPOs(ORolePermissionRelationPO rolePermissionRelationPO) {
		String jpql = "from ORolePermissionRelationPO u where 1=1";
		if(!StringUtil.isEmpty(rolePermissionRelationPO.getAvailable())){
			jpql += " and u.available = '" + String.valueOf(rolePermissionRelationPO.getAvailable())+"'";
		}
		if(!StringUtil.isEmpty(rolePermissionRelationPO.getRolePO())){
			jpql += " and u.rolePO.id = " + rolePermissionRelationPO.getRolePO().getId();
		}
		if(!StringUtil.isEmpty(rolePermissionRelationPO.getPermissionPO())){
			jpql += " and u.permissionPO.id = " + rolePermissionRelationPO.getPermissionPO().getId();
		}
		return baseDao.getResultList(jpql);
	}

	@Override
	public List<ORolePermissionRelationPO> findPermissionRelationPOsByJpql(String jpql) {
		return baseDao.getResultList(jpql);
	}

}
