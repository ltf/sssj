package main.java.login.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.java.login.dao.IRoleDao;
import main.java.login.entity.ORolePO;
import main.java.util.BaseDao;

@Repository
public class RoleDaoImpl implements IRoleDao{

	@Autowired
	private BaseDao baseDao;
	
	@Override
	public BaseDao getBaseDao() {
		return baseDao;
	}

	@Override
	public void save(ORolePO rolePO) {
		baseDao.save(rolePO);
	}

	@Override
	public ORolePO findRolePOById(Long id) {
		return baseDao.findEntity(ORolePO.class, id);
	}

}
