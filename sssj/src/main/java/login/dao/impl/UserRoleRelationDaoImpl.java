package main.java.login.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.java.login.dao.IUserRoleRelationDao;
import main.java.login.entity.OUserRoleRelationPO;
import main.java.util.BaseDao;
import main.java.util.StringUtil;

@Repository
public class UserRoleRelationDaoImpl implements IUserRoleRelationDao{

	@Autowired
	private BaseDao baseDao;
	
	@Override
	public void save(OUserRoleRelationPO userRoleRelationPO) {
		baseDao.save(userRoleRelationPO);
	}

	@Override
	public List<OUserRoleRelationPO> findUserRoleRelationPO(OUserRoleRelationPO userRoleRelationPO) {
		
		String jpql = "from OUserRoleRelationPO u where 1=1";
		if(!StringUtil.isEmpty(userRoleRelationPO.getAvailable())){
			jpql += " and u.available = '" +String.valueOf(userRoleRelationPO.getAvailable())+ "'";
		}
		if(!StringUtil.isEmpty(userRoleRelationPO.getRolePO())){
			jpql += " and u.rolePO.id = " + userRoleRelationPO.getRolePO().getId();
		}
		if(!StringUtil.isEmpty(userRoleRelationPO.getUserPO())){
			jpql += " and u.userPO.id = " + userRoleRelationPO.getUserPO().getId();
		}
		return baseDao.getResultList(jpql);
	}

	@Override
	public OUserRoleRelationPO update(OUserRoleRelationPO userRoleRelationPO) {
		return baseDao.saveOrUpdateEntity(userRoleRelationPO);
	}

	@Override
	public List<OUserRoleRelationPO> findRelationPOsByJpql(String jpql) {
		return baseDao.getResultList(jpql);
	}

	@Override
	public BaseDao getBaseDao() {
		return baseDao;
	}

}
