package main.java.login.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.java.login.dao.IPermissionDao;
import main.java.login.entity.OPermissionPO;
import main.java.util.BaseDao;
import main.java.util.StringUtil;

@Repository
public class PermissionDaoImpl implements IPermissionDao{

	@Autowired
	private BaseDao baseDao;
	
	@Override
	public void save(OPermissionPO permissionPO) {
		baseDao.save(permissionPO);
	}

	@Override
	public OPermissionPO findPermissionPOById(Long id) {
		return baseDao.findEntity(OPermissionPO.class, id);
	}

	@Override
	public List<OPermissionPO> findPermissionPOByPermission(OPermissionPO permissionPO) {
		String jpql = "from OPermissionPO u where 1=1";
		if(!StringUtil.isEmpty(permissionPO.getId())){
			jpql += " and u.id = " + permissionPO.getId();
		}
		if(!StringUtil.isEmpty(permissionPO.getAvailable())){
			jpql += " and u.available = '" +String.valueOf(permissionPO.getAvailable())+ "'";
		}
		return baseDao.getResultList(jpql);
	}

	@Override
	public BaseDao getBaseDao() {
		return this.baseDao;
	}

}
