package main.java.login.dao;

import java.util.List;

import main.java.login.entity.OPermissionPO;
import main.java.util.BaseDao;

public interface IPermissionDao {
	
	public BaseDao getBaseDao();

	/**
	 * 
	 * 2014��12��2�� ����3:29:25
	 * @param permissionPO
	 */
	public void save(OPermissionPO permissionPO);
	
	
	public OPermissionPO findPermissionPOById(Long id);
	
	public List<OPermissionPO> findPermissionPOByPermission(OPermissionPO permissionPO);
	
}
