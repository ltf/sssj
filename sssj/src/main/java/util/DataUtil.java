package main.java.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;

public class DataUtil {

    private static final Logger logger = Logger.getLogger(DataUtil.class);
    public static final String DEFAULT_PATTERN = "#0.0000";

    /**
     * 格式化数字
     * 
     * @Title: getNumberFormat
     * @version V1.0
     * @return String
     */
    public static String getNumberFormat(BigDecimal num) {
//    	if(num == null || BigDecimal.ZERO.compareTo(num) == 0){
//    		return "0.0000";
//    	}
//        String number = new DecimalFormat("#.0000").format(num);
//        return number;
    	logger.info("");
    	return getNumberFormat(num, DEFAULT_PATTERN);
    }
    
    public static String getNumberFormat(BigDecimal num, String pattern){
    	if (num == null) {
			num = BigDecimal.ZERO;
		}
    	if (pattern != null) {
			return new DecimalFormat(pattern).format(num);
		}else {
			return new DecimalFormat(DEFAULT_PATTERN).format(num);
		}
    }

    public static void main(String[] args) {
        BigDecimal num = new BigDecimal(3);
        System.out.println(getNumberFormat(null));
        System.out.println(getNumberFormat(num));
        System.out.println(getNumberFormat(BigDecimal.valueOf(.45)));
    }
    
    public static BigDecimal getNumberFormatBig(BigDecimal num){
    	if(num == null){
    		num = BigDecimal.ZERO;
    	}
    	String number = new DecimalFormat("#.0000").format(num);
    	return new BigDecimal(number);
    }
    
    public static BigDecimal getNumberFormatBig(BigDecimal num , String pattern){
    	if(num == null){
    		num = BigDecimal.ZERO;
    	}
    	if(pattern == null){
    		pattern = "#0.0000";
    	}
    	String number = new DecimalFormat(pattern).format(num);
    	return new BigDecimal(number);
    }
}
