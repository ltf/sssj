package main.java.util;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.jdbc.core.JdbcTemplate;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

/**   
* @Title: ChartJSUtil.java 
* @version V1.0   
*/ 
public class ChartJSUtil {

	/** 
	* @Title: listToColumnChart 
	* @Description: 将传入的list转换成柱状图所需的json
	* @version V1.0 
	* @param manageList 
	* 				要处理的list
	* @param x
	* 				X轴（横坐标）变量名
	* @param y
	* 				Y轴（纵坐标）变量名
	* @param group
	* 				柱状图分类名
	* @param clz
	* 				数据类型，"1":Integer  "2":BigDecimal  
	* @return
	*        String 柱状图所需的json字符串
	*        
	*<p>   
	* 例：
	* 	要显示一年中每个月每种药品的销售图，则
	* 	先要传入list,里面每一个map包含  月份，药品名称，数量
	* 	x参数传入月份的字段名
	* 	y参数传入销售数量字段名
	* 	group参数传入药品名称字段名
	* 
	* 	将返回的字符串输出到ajax的success方法中，
	*		 xAxis: {
	*            categories: 此处传入x的内容
	*       }
	*       
	*   series: 此处要传入y的内容
	* </p>
	*/ 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String listToColumnChart(List<Map> manageList,String x,String y,String group,String clz){
		List<String> xList = new ArrayList<String>();
		Map<String,Map> groupYMap = new HashMap<String, Map>();
		for (Map map : manageList) {
			String xValue = String.valueOf(map.get(x));
			if(!xList.contains(xValue)){
				xList.add(xValue);
			}
			String groupValue = String.valueOf(map.get(group));
			if("1".equals(clz)){
				Map<String,Integer> groupYValue = groupYMap.get(groupValue);
				if(groupYValue == null){
					groupYValue = new LinkedHashMap<String,Integer>();
				}
				groupYValue.put(xValue, Integer.parseInt(String.valueOf(map.get(y))));
				groupYMap.put(groupValue, groupYValue);
			}
			if("2".equals(clz)){
				Map<String,BigDecimal> groupYValue = groupYMap.get(groupValue);
				if(groupYValue == null){
					groupYValue = new LinkedHashMap<String,BigDecimal>();
				}
				groupYValue.put(xValue, new BigDecimal(String.valueOf(map.get(y))));
				groupYMap.put(groupValue, groupYValue);
			}
			
		}
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		Set<String> drugSet = groupYMap.keySet();
		for (String drugName : drugSet) {
			Map<String,Object> tempMap = new HashMap<String,Object>();
			if("1".equals(clz)){
				List<Integer> quatityList = new ArrayList<Integer>();
				for (String insertDate : xList) {
					Map<String,Integer> dateQuatityMap = groupYMap.get(drugName);
					quatityList.add(StringUtil.isEmpty(dateQuatityMap.get(insertDate)) ? 0 : dateQuatityMap.get(insertDate));
				}
				tempMap.put("name",drugName);
				tempMap.put("data", quatityList);
			}
			if("2".equals(clz)){
				List<BigDecimal> quatityList = new ArrayList<BigDecimal>();
				for (String insertDate : xList) {
					Map<String,BigDecimal> dateQuatityMap = groupYMap.get(drugName);
					quatityList.add(StringUtil.isEmpty(dateQuatityMap.get(insertDate)) ? new BigDecimal(0) : dateQuatityMap.get(insertDate));
				}
				tempMap.put("name",drugName);
				tempMap.put("data", quatityList);
			}
			
			resultList.add(tempMap);
		}
		String xListStr = JSONUtil.toJSON(xList);
		String resultListStr = JSONUtil.toJSON(resultList);
		String resultJSONStr = "{\""+x+"\":" + xListStr + ",\""+y+"\":" + resultListStr +"}";
		return resultJSONStr;
	}
	
	@SuppressWarnings("deprecation")
	public static void printPDFReport(HttpServletRequest request,
			HttpServletResponse response,
			JdbcTemplate jdbcTemplate,
			String jasperFilePath,
			Map<String,Object> parameters
			){
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		File exe_rpt = new File(request.getRealPath(jasperFilePath)); 
		try
		{ 
			// 生成pdf 
			byte[] bytes = null;
			if(jdbcTemplate != null){
				bytes = JasperRunManager.runReportToPdf(exe_rpt.getPath(), parameters, jdbcTemplate.getDataSource().getConnection());
			}
			else{
				bytes= JasperRunManager.runReportToPdf(exe_rpt.getPath(), parameters,new JREmptyDataSource());
			}
			
			response.setHeader("Content-Type", "text/html;charset=GBK");
			response.setContentType("application/pdf"); 
			response.setContentLength(bytes.length); 
			ServletOutputStream ouputStream = response.getOutputStream(); 
			ouputStream.write(bytes,0,bytes.length); 
			ouputStream.flush(); 
			ouputStream.close(); 
		}
		catch(JRException ex)
		{ 
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	@SuppressWarnings("deprecation")
	public static void printPDFFileReport(HttpServletRequest request,
			HttpServletResponse response,
			JdbcTemplate jdbcTemplate,
			String jasperFilePath,
			Map<String,Object> parameters
			){
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		File exe_rpt = new File(request.getRealPath(jasperFilePath)); 
		try
		{ 
			// 生成pdf 
			String pdfStr = "";
			if(jdbcTemplate != null){
				pdfStr = JasperRunManager.runReportToPdfFile(exe_rpt.getPath(), parameters, jdbcTemplate.getDataSource().getConnection());
			}
			else{
				pdfStr= JasperRunManager.runReportToPdfFile(exe_rpt.getPath(), parameters);
			}
			byte[] bytes = pdfStr.getBytes();
			response.setHeader("Content-Type", "text/html;charset=GBK");
			response.setContentType("application/pdf"); 
			response.setContentLength(bytes.length); 
			ServletOutputStream ouputStream = response.getOutputStream(); 
			ouputStream.write(bytes,0,bytes.length); 
			ouputStream.flush(); 
			ouputStream.close(); 
		}
		catch(JRException ex)
		{ 
			System.out.print("Jasper Output Error:"+ex.getMessage()); 
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	@SuppressWarnings("deprecation")
	public static void printHtmlReport(HttpServletRequest request,
			HttpServletResponse response,
			JdbcTemplate jdbcTemplate,
			String jasperFilePath,
			Map<String,Object> parameters
			){
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		File exe_rpt = new File(request.getRealPath(jasperFilePath));
		String str = "";
		try
		{ 
			if(jdbcTemplate == null){
				str = JasperRunManager.runReportToHtmlFile(exe_rpt.getPath(), parameters);
			}
			else{
				str = JasperRunManager.runReportToHtmlFile(exe_rpt.getPath(), parameters,jdbcTemplate.getDataSource().getConnection());
			}
			response.setHeader("Content-Type", "text/html;charset=UTF-8");
			response.setContentType("text/html"); 
			response.setContentLength(str.length()); 
			ServletOutputStream ouputStream = response.getOutputStream(); 
			byte[] bytedata = str.getBytes();
			ouputStream.write(bytedata,0,str.length()); 
			ouputStream.flush(); 
			ouputStream.close(); 
		}
		catch(Exception e){
			e.printStackTrace();
		}
			
	}

	@SuppressWarnings("deprecation")
	public static void printPDFReport(HttpServletRequest request,
			HttpServletResponse response, JRDataSource orderDataSource,
			String jasperFilePath,Map<String,Object> parameters) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		File exe_rpt = new File(request.getRealPath(jasperFilePath)); 
		try
		{ 
			// 生成pdf 
			byte[] bytes = null;
			if(orderDataSource != null){
				bytes = JasperRunManager.runReportToPdf(exe_rpt.getPath(), parameters, orderDataSource);
			}
			else{
				bytes= JasperRunManager.runReportToPdf(exe_rpt.getPath(), parameters,new JREmptyDataSource());
			}
			
			response.setHeader("Content-Type", "text/html;charset=GBK");
			response.setContentType("application/pdf"); 
			response.setContentLength(bytes.length); 
			ServletOutputStream ouputStream = response.getOutputStream(); 
			ouputStream.write(bytes,0,bytes.length); 
			ouputStream.flush(); 
			ouputStream.close(); 
		}
		catch(JRException ex)
		{ 
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
//	/**
//	 * 输出报表文件
//	 * @param request
//	 * @param orderDataSource  数据源
//	 * @param exportFilePath   输出目标文件
//	 * @param jasperFilePath   jasper文件
//	 * @param parameters
//	 * @param type 文件类型  目前只接受 excel
//	 * 
//	 */
//	public static void printFile(HttpServletRequest request,
//			JRDataSource orderDataSource,String exportFilePath,
//			String jasperFilePath,Map<String,Object> parameters,String type){
//		try {
//			request.setCharacterEncoding("UTF-8");
//		} catch (UnsupportedEncodingException e1) {
//			e1.printStackTrace();
//		}
//		File exe_rpt = new File(request.getRealPath(jasperFilePath)); 
//		try {
//			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new FileInputStream(exe_rpt));
//			prepareReport(jasperReport, type);
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, orderDataSource);
//			FileOutputStream ouputStream = new FileOutputStream(new File(exportFilePath));
//			JRXlsExporter exporter = new JRXlsExporter();
//		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//		    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,ouputStream);
//		    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
//		    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
//		    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
//		    exporter.exportReport();
//		    ouputStream.flush();
//		    ouputStream.close();
//			
////			JasperFillManager.fillReportToFile(exe_rpt.getPath(), excelFilePath, parameters, orderDataSource);
//		} catch (JRException e) {
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//	}
//
//	private static void prepareReport(JasperReport jasperReport, String type) {
//		if ("excel".equals(type)){
//			try {
//				Field margin = JRBaseReport.class.getDeclaredField("leftMargin");
//		        margin.setAccessible(true);
//		        margin.setInt(jasperReport, 0);
//		        margin = JRBaseReport.class.getDeclaredField("topMargin");
//		        margin.setAccessible(true);
//		        margin.setInt(jasperReport, 0);
//		        margin = JRBaseReport.class.getDeclaredField("bottomMargin");
//		        margin.setAccessible(true);
//		        margin.setInt(jasperReport, 0);
//		        Field pageHeight = JRBaseReport.class.getDeclaredField("pageHeight");
//		        pageHeight.setAccessible(true);
//		        pageHeight.setInt(jasperReport, 2147483647);
//			} catch (Exception e) {
//			}
//		}
//	}
	
	@SuppressWarnings("deprecation")
	public static void printExcelReport(HttpServletRequest request,
			HttpServletResponse response, JRDataSource orderDataSource,
			String jasperFilePath,Map<String,Object> parameters,String fileName){
		try {
			response.setCharacterEncoding("UTF-8");  
			//得到jasper文件
			File jasperFile=new File(request.getRealPath(jasperFilePath));
			JasperReport jasperReport= (JasperReport)JRLoader.loadObject(jasperFile);
			JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport, parameters, orderDataSource);
			JRXlsExporter exporter=new JRXlsExporter();
			exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

			response.setHeader("Content-Disposition", "attachment;filename="+fileName+".xls");
			response.setContentType("application/vnd_ms-excel");
			exporter.exportReport();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		} 
	}
}
